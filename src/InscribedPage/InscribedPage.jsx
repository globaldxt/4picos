import React from 'react';
import Header from '../_components/Header';
import Button from '../_components/Button';
import Footer from '../_components/Footer';
import ImageHero from './image1.jpg';
import './InscribedPage.css';
import { FormattedMessage } from 'react-intl';
import Media from "react-media";
import HeaderMobile from '../_components/HeaderMobile';
//import SimpleMap from './SimpleMap.jsx';
import Map from './InscribedMap.jsx';

class HostingPage extends React.Component {
  render() {
    return (
      <div className="Hosting">
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              <Header />
            ) : (
              <HeaderMobile />
            )}
        </Media>
        <section className="Hosting-hero"
          style={{
            height: '350px',
            width: '100%',
            backgroundImage: `url(${ImageHero})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top'
          }}>
        </section>
        <section className="Hosting-banner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <p>
                  <FormattedMessage id="rules.banner" defaultMessage="Inscripciones abiertas por aplazamiento de la edición 2020" />
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="Hosting-info">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <h3 className="Hosting-title">Mapa de inscritos</h3>
              </div>
            </div>
            <div className="container py-4">
              <div className="row py-4">
                <div className="col-12 py-4">
                  <h6 className="text-message">
                    <FormattedMessage id="wear.message2" defaultMessage="Consulta nuestro mapa de inscritos por provincia. También puedes acceder al listado completo de inscritos desde este" />
                  </h6>
                  <h6 className="text-message">
                    <a href="https://eventos.emesports.es/inscripcion/pontevedra-4picos-bike-2020/participantes/?iframe=1&lang=es&background=transparent" target="_blank" rel="noopener noreferrer">
                      <FormattedMessage id="wear.message" defaultMessage="enlace" />
                    </a>
                  </h6>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="map">
          <div className="container">
            <div className="col-12">
              <Map />
            </div>
          </div>
        </section>
        <section className="space"></section>
        <Footer />
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              ''
            ) : (
              <section className="inscription-fixed-bar">
                <Button className="inscription-fixed" href="https://eventos.emesports.es/inscripcion/pontevedra-4picos-bike-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent" target="_blank" rel="noopener noreferrer">
                  Inscríbete
                </Button>
              </section>
            )}
        </Media>
      </div>
    );
  }
}

export default HostingPage;