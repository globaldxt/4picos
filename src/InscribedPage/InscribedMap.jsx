import React from 'react'
import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';
import { renderToStaticMarkup } from "react-dom/server";
import { divIcon } from "leaflet";

class Map extends React.Component {
  render() {
    const provinces = [
      { name: 'A CORUÑA', inscribed: 399, lat: '43.3618688', lng: '-8.4477032' },
      { name: 'ÁLAVA', inscribed: 1, lat: '42.8432454', lng: '-3.3201853' },
      { name: 'ALBACETE', inscribed: 0, lat: '38.992194', lng: '-1.895609' },
      { name: 'ALICANTE', inscribed: 1, lat: '38.3578356', lng: '-0.5075437' },
      { name: 'ALMERÍA', inscribed: 1, lat: '36.841523', lng: '-2.4921362' },
      { name: 'ASTURIAS', inscribed: 4, lat: '43.269062', lng: '-6.9677499' },
      { name: 'ÁVILA', inscribed: 0, lat: '40.6568848', lng: '-4.6997033' },
      { name: 'BADAJOZ', inscribed: 1, lat: '38.8793748', lng: '-7.0226987' },
      { name: 'BALEARES', inscribed: 0, lat: '39.353396', lng: '1.6143707' },
      { name: 'BARCELONA', inscribed: 0, lat: '41.3947688', lng: '2.0787275' },
      { name: 'BURGOS', inscribed: 2, lat: '42.3441207', lng: '-3.7297126' },
      { name: 'CÁCERES', inscribed: 0, lat: '39.4716313', lng: '-6.4257388' },
      { name: 'CÁDIZ', inscribed: 1, lat: '36.5163813', lng: '-6.3174868' },
      { name: 'CANTABRIA', inscribed: 5, lat: '43.1345037', lng: '-4.5611725' },
      { name: 'CASTELLÓN', inscribed: 0, lat: '39.9874581', lng: '-0.0655726' },
      { name: 'CEUTA', inscribed: 0, lat: '35.8890513', lng: '-5.3535454' },
      { name: 'CIUDAD REAL', inscribed: 0, lat: '38.9860385', lng: '-3.9620075' },
      { name: 'CÓRDOBA', inscribed: 0, lat: '37.8915392', lng: '-4.8195049' },
      { name: 'CUENCA', inscribed: 0, lat: '39.937276', lng: '-3.277569' },
      { name: 'GIRONA', inscribed: 1, lat: '41.9802474', lng: '2.7836477' },
      { name: 'GRANADA', inscribed: 0, lat: '37.1809411', lng: '-3.6262913' },
      { name: 'GUADALAJARA', inscribed: 10, lat: '40.6321888', lng: '-3.1906821' },
      { name: 'GUIPÚZCOA', inscribed: 2, lat: '43.1452359', lng: '-2.446204' },
      { name: 'HUELVA', inscribed: 0, lat: '37.2708627', lng: '-6.9747099' },
      { name: 'HUESCA', inscribed: 1, lat: '42.135905', lng: '-0.423358' },
      { name: 'JAÉN', inscribed: 0, lat: '37.7800892', lng: '-3.8318846' },
      { name: 'LA RIOJA', inscribed: 0, lat: '42.2802849', lng: '-2.9669464' },
      { name: 'LAS PALMAS', inscribed: 12, lat: '28.1173563', lng: '-15.4746367' },
      { name: 'LEÓN', inscribed: 36, lat: '42.6036003', lng: '-5.6124108' },
      { name: 'LLEIDA', inscribed: 0, lat: '41.618337', lng: '0.5849153' },
      { name: 'LUGO', inscribed: 73, lat: '43.0122783', lng: '-7.5915196' },
      { name: 'MADRID', inscribed: 17, lat: '40.4378698', lng: '-3.8196226' },
      { name: 'MÁLAGA', inscribed: 0, lat: '36.7647499', lng: '-4.5642757' },
      { name: 'MELILLA', inscribed: 0, lat: '35.2862811', lng: '-2.9620361' },
      { name: 'MURCIA', inscribed: 0, lat: '37.9805272', lng: '-1.1621949' },
      { name: 'NAVARRA', inscribed: 0, lat: '42.6068763', lng: '-2.7332341' },
      { name: 'OURENSE', inscribed: 101, lat: '42.3383573', lng: '-7.8987051' },
      { name: 'PALENCIA', inscribed: 0, lat: '42.0088108', lng: '-4.5619734' },
      { name: 'PONTEVEDRA', inscribed: 500, lat: '42.4338595', lng: '-8.6568553' },
      {name: 'PRINCIPADO ANDORRA', inscribed: 0, lat: '42.5423751', lng: '1.527633'},
      {name: 'SALAMANCA', inscribed: 5, lat: '40.9634332', lng: '-5.7042317' },
      {name: 'SANTA CRUZ DE TENERIFE', inscribed: 0, lat: '28.4578159', lng: '-16.3213542' },
      {name: 'SEGOVIA', inscribed: 0, lat: '40.9412449', lng: '-4.1472215' },
      {name: 'SEVILLA', inscribed: 0, lat: '37.3753501', lng: '-6.0250988' },
      {name: 'SORIA', inscribed: 0, lat: '41.7671202', lng: '-2.4920887' },
      {name: 'TARRAGONA', inscribed: 1, lat: '41.1257995', lng: '1.2035639' },
      {name: 'TERUEL', inscribed: 0, lat: '40.3450083', lng: '-1.1184745' },
      {name: 'TOLEDO', inscribed: 5, lat: '39.7829834', lng: '-5.2784646' },
      {name: 'VALENCIA', inscribed: 0, lat: '39.4077012', lng: '-0.5015974' },
      {name: 'VALLADOLID', inscribed: 2, lat: '41.7031684', lng: '-4.9488979' },
      {name: 'VIZCAYA', inscribed: 0, lat: '43.2192198', lng: '-3.2111302' },
      {name: 'ZAMORA', inscribed: 1, lat: '41.5038085', lng: '-5.7620457' },
      {name: 'ZARAGOZA', inscribed: 0, lat: '41.6516859', lng: '-0.9300005' },
      {name: 'PORTUGAL', inscribed: 68, lat: '39.1207361', lng: '-8.5337037' }
    ]
    
    const markers = provinces
      .filter((province) => { return province.inscribed > 0; })
      .map((province) => 
        <Marker
          position={[province.lat, province.lng]}
          icon={divIcon({
            html: renderToStaticMarkup(
              <div style={{
                color: 'white',
                background: '#c21718',
                padding: '10px',
                height: '30px',
                width: '30px',
                borderRadius: '50%',
                display: 'inline-flex',
                textAlign: 'center',
                alignItems: 'center',
                justifyContent: 'center',
                transform: 'translate(-50%, -50%)',
                fontSize: '14px',
                lineHeight: '14px'
              }}>{province.inscribed}</div>
            )
          })}
        >
          <Popup>{province.name}</Popup>
        </Marker>
      );

    return (
      <LeafletMap
        center={[40.433689, -3.708386]}
        zoom={6}
        maxZoom={10}
        attributionControl={true}
        zoomControl={true}
        doubleClickZoom={true}
        scrollWheelZoom={false}
        dragging={true}
        animate={true}
        easeLinearity={0.35}
      >
        <TileLayer
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
        />
        {markers}
      </LeafletMap>
    );
  }
}

export default Map