import React from "react";
import Header from "../_components/Header";
import Button from "../_components/Button";
import Footer from "../_components/Footer";
import ImageHero from "./image1.jpg";
import Image2 from "./image2.jpg";
import "./RatesPage.css";
import { FormattedMessage } from "react-intl";
import Media from "react-media";
import HeaderMobile from "../_components/HeaderMobile";

class RatesPage extends React.Component {
  render() {
    return (
      <div className="Rates">
        <Media query={{ minWidth: 768 }}>
          {matches => (matches ? <Header /> : <HeaderMobile />)}
        </Media>
        <section
          className="Rates-hero"
          style={{
            height: "350px",
            width: "100%",
            backgroundImage: `url(${ImageHero})`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "top"
          }}
        ></section>
        <section className="Rates-banner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <p>
                  <FormattedMessage
                    id="rates.banner"
                    defaultMessage="Inscripciones abiertas por aplazamiento de la edición 2020"
                  />
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="Rates-info">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <h3 className="Rates-title">
                  <FormattedMessage id="rates.title" defaultMessage="Tarifas" />
                </h3>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 col-lg-6">
                <article className="Rates-info-article">
                  <p className="Rates-info-text text-justify">
                  Las inscripciones continúan abiertas en la pagina web citada y con cierre de las mismas el día 25 de agosto de 2021 o hasta completar <strong>aforo máximo de 1800</strong> participantes en todas las categorías de BTT.
                  </p>
                  <p className="Rates-info-text text-justify">La tarifa de inscripción es de <strong><u>40 euros iva incluído</u></strong> independientemente de la modalidad de participación escogida. La tarifa se incrementa 30 euros en caso de querer el <strong>maillot o chaleco conmemorativo</strong> y 45 euros en el caso del culotte.</p>
                  {/* <table className="table table-responsive">
                    <tbody>
                      <tr>
                        <th className="scope">
                          <FormattedMessage
                            id="rates.first.step"
                            defaultMessage="Del 22 al 28 de noviembre de 2019"
                          />
                        </th>
                        <td>27 €</td>
                      </tr>
                      <tr>
                        <th className="scope">
                          <FormattedMessage
                            id="rates.second.step"
                            defaultMessage="Del 29 de noviembre al 31 de diciembre de 2019"
                          />
                        </th>
                        <td>30 €</td>
                      </tr>
                      <tr>
                        <th className="scope">
                          <FormattedMessage
                            id="rates.third.step"
                            defaultMessage="Del 1 de enero al 29 de febrero de 2020"
                          />
                        </th>
                        <td>35 €</td>
                      </tr>
                      <tr>
                        <th className="scope">
                          <FormattedMessage
                            id="rates.fourth.step"
                            defaultMessage="Del 1 de marzo al 23 de agosto de 2020"
                          />
                        </th>
                        <td>40 €</td>
                      </tr>
                    </tbody>
                  </table> */}
                  {/* <p className="small">
                    ***la tarifa se incrementa 29 euros en caso de querer el{" "}
                    <strong>maillot o chaleco conmemorativo</strong> y 45 euros
                    en el caso del culotte, por persona.
                  </p> */}
                  <h6>
                  Se establecen los siguientes PERIODOS DE DEVOLUCIÓN DE LAS INSCRIPCIONES:
                  </h6>
                  <ul>
                    <li className="small">100% Hasta el 30 de junio de 2021</li>
                    <li className="small">50% desde el 1 de julio al 1 de agosto de 2021</li>
                    <li className="small">25% desde el 2 de agosto al 25 de agosto de 2021</li>
                    <li className="small">0% desde el 26 de agosto de 2021</li>
                  </ul>
                  <p className="small"><strong>SOLO SE REALIZARÁN LAS DEVOLUCIONES INDICADAS EN LOS PERIODOS MAS ARRIBA ESTABLECIDOS, SEA LA CAUSA QUE SEA POR LA QUE SE SOLICITA DICHA DEVOLUCIÓN. (ni por causa médica) </strong></p>
                  <p className="small">
                    <strong><u>*Excepto 2 euros de gastos de devolución y gestión</u></strong>
                  </p>
                  <div className="Rates-inscription-button-wrapper">
                    <a
                      className="Rates-inscription-button"
                      href="https://eventos.emesports.es/inscripcion/pontevedra-4picos-bike-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <FormattedMessage
                        id="rates.enroll"
                        defaultMessage="Inscríbete"
                      />
                    </a>
                  </div>
                </article>
              </div>
              <div className="col-md-12 col-lg-6">
                <div
                  className="Rates-info-img"
                  style={{
                    backgroundImage: `url(${Image2})`,
                    backgroundRepeat: "no-repeat",
                    backgroundPosition: "top center",
                    backgroundSize: "cover",
                    height: "800px",
                    marginRight: "-15px",
                    marginLeft: "-15px"
                  }}
                ></div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              ""
            ) : (
              <section className="inscription-fixed-bar">
                <Button
                  className="inscription-fixed"
                  href="https://eventos.emesports.es/inscripcion/pontevedra-4picos-bike-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  Inscríbete
                </Button>
              </section>
            )
          }
        </Media>
      </div>
    );
  }
}

export default RatesPage;
