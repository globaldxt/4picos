import React from 'react';
import Header from '../_components/Header';
import Button from '../_components/Button';
import Footer from '../_components/Footer';
import ImageHero from './image1.jpg';
import 'react-html5video/dist/styles.css';
import './TracksPage.css';
import { FormattedMessage } from 'react-intl';
import Media from "react-media";
import HeaderMobile from '../_components/HeaderMobile';

class TracksPage extends React.Component {
  render() {
    return (
      <div className="Tracks">
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              <Header />
            ) : (
              <HeaderMobile />
            )}
        </Media>
        <section className="Tracks-hero"
          style={{
            height: '350px',
            width: '100%',
            backgroundImage: `url(${ImageHero})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top'
          }}>
        </section>
        <section className="Tracks-banner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <p>
                  <FormattedMessage id="rules.banner" defaultMessage="Inscripciones abiertas por aplazamiento de la edición 2020" />
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="Tracks-info">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <h3 className="Tracks-title">Multimedia</h3>
              </div>
            </div>
            {/* <div className="row py-5">
              <div className="col-12">
                <Video
                  controls={['PlayPause', 'Seek', 'Time', 'Volume', 'Fullscreen']}
                  onCanPlayThrough={() => {
                    // Do stuff
                  }}>
                  <source src={promo} type="video/mp4" />
                </Video>
              </div>
            </div> */}
          </div>
        </section>
        <section className="video my-5">
          <div className="container">
            <div className="row">
              <div className="col-12 col-md-8 offset-md-2">
                <h4>Próximamente</h4>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              ''
            ) : (
              <section className="inscription-fixed-bar">
                <Button className="inscription-fixed" href="https://eventos.emesports.es/inscripcion/pontevedra-4picos-bike-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent" target="_blank" rel="noopener noreferrer">
                  Inscríbete
                </Button>
              </section>
            )}
        </Media>
      </div>
    );
  }
}

export default TracksPage;