import React from 'react';
import { Route } from 'react-router-dom';
import HomePage from '../HomePage';
import GalleryPage from '../GalleryPage';
import RatesPage from '../RatesPage';
import RulesPage from '../RulesPage';
import ContactPage from '../ContactPage';
import MediaPage from '../MediaPage';
import EmbassadorPage from '../EmbassadorPage';
import TracksPage from '../TracksPage';
import WearPage from '../WearPage';
import HostingPage from '../HostingPage';
import InscribedPage from '../InscribedPage';
import SponsorsPage from '../SponsorsPage';

class App extends React.Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={HomePage} />
        <Route path="/tarifas" component={RatesPage} />
        <Route path="/reglamento" component={RulesPage} />
        <Route path="/multimedia" component={MediaPage} />
        <Route path="/galeria" component={GalleryPage} />
        <Route path="/contacto" component={ContactPage} />
        <Route path="/embajadores" component={EmbassadorPage} />
        <Route path="/tracks" component={TracksPage} />
        <Route path="/ropa" component={WearPage} />
        <Route path="/alojamiento" component={HostingPage} />
        <Route path="/inscritos" component={InscribedPage} />
        <Route path="/sponsors" component={SponsorsPage} />
      </div>
    );
  }
}

export default App;