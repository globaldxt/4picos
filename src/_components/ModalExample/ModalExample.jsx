import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

const ModalExample = (props) => {
  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(true);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="danger" onClick={toggle}>{buttonLabel}</Button>
      <Modal size="lg" isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>ESTADO DE ALERTA CORONAVIRUS</ModalHeader>
        <ModalBody>
          <p>Debido a las circunstancias excepcionales que estamos viviendo por el estado de Alerta, la prueba se ha tenido que aplazar al 15 de mayo de 2021 (pendiente de confirmación por parte de las autoridades) y por ello se hace preciso regular  las modalidades de cambio de inscripciones debido al cambio de fecha inicial de la prueba, para ello se ofrecen las siguientes posibilidades:</p>
          <ol>
            <li><strong>Cambio automático de la inscripción para la nueva fecha 15 de mayo de 2021.</strong> En este caso no tendríais que hacer nada, la organización realiza el cambio automáticamente. </li><br/>
            <li>Aquellos que no puedan realizar la prueba, <strong>se le permite excepcionalmente un cambio de titularidad de su plaza</strong>, manteniendo la tarifa que han pagado. En este caso habilitaremos el formato de cambio y será puesto en conocimiento de todos los participantes. Aquellos que cambien su plaza, deben recordar que las tallas de ropa elegida por el participante original se mantienen. <br/>Para proceder al cambio entra en el siguiente <strong><a href="https://eventos.emesports.es/inscripcion/pontevedra-4picos-bike-2020/zona_privada/logeo/?iframe=1&lang=es&background=FFFFFF&utm_nooverride=1">enlace.</a></strong></li><br/>
            <li>Aquellos que lo deseen, pueden solicitar la devolución del 100% de la tarifa de inscripción. Esta opción debe comunicarse antes del 15 de abril. A partir el 15 de abril y hasta al 1 de marzo, se devolverá el 25% de la tarifa. En caso de querer dicha devolución, se debe poner un mail a soporte@emesports.es indicando datos personales y que se solicita la <strong>DEVOLUCION DE TARIFA</strong>.</li><br/>
          </ol>
          <p><strong>CONSULTA EL REGLAMENTO PARA COMPROBAR TODAS LAS VARIACIONES</strong></p>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default ModalExample;