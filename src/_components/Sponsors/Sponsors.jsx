import React from 'react';
import SponsorsContainer from './SponsorsContainer';
import SponsorWrapper from './SponsorWrapper';
import Sponsor from './Sponsor';
import sponsor1 from './img/xacobeo2021.jpg';
import sponsor2 from './img/concello-pontevedra.png';
import sponsor3 from './img/depo.jpg';
import sponsor4 from './img/froiz.jpg';
import sponsor5 from './img/bikeshop.png';
import sponsor6 from './img/vozysonido.png';
import sponsor7 from './img/gesmagal.png';
import sponsor8 from './img/rawRainbow.png';
import sponsor9 from './img/academy.png';
import sponsor10 from './img/caetano.png';
import sponsor11 from './img/beone.png';
import sponsor12 from './img/bifrutas.png';
import sponsor13 from './img/millabikes.png';
import sponsor14 from './img/pascual.png';
import sponsor15 from './img/spiuk.png';
import sponsor16 from './img/towcar.png';
import sponsor17 from './img/xeve.png';
import sponsor18 from './img/xsauce.png';
import sponsor19 from './img/ottobosley.png';
import sponsor20 from './img/pirelli.png';


class Sponsors extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [sponsor1, sponsor2, sponsor3, sponsor4, sponsor5, sponsor6, sponsor7, sponsor8, sponsor9, sponsor10, sponsor11, sponsor12, sponsor13, sponsor14, sponsor15, sponsor16, sponsor17, sponsor18, sponsor19, sponsor20],
      currentIndex: 8,
      interval: null
    }
  }

  componentDidMount = () => {
    const interval = window.setInterval(() => {
      this.goToNextSponsor()
    }, 2000)
    this.setState({ interval })
  }

  goToNextSponsor = () => {
    // if (this.state.currentIndex === this.state.images.length - 1) {
    //   return this.setState({
    //     currentIndex: 0,
    //     translateValue: 0
    //   })
    // }

    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      translateValue: prevState.translateValue + -(this.sponsorWidth()),
      images: [...prevState.images.slice(1), prevState.images[0]]
    }));
  }

  sponsorWidth = () => {
    return document.querySelector('.sponsor-wrapper').clientWidth
  }

  render() {
    return (
      <SponsorsContainer>
        {
          this.state.images.map((image, i) => (
            <SponsorWrapper key={i} className="sponsor-wrapper" style={{
              transform: `translateX(${this.state.translateValue}px)`
            }}>
              <Sponsor src={image} />
            </SponsorWrapper>
          ))
        }
      </SponsorsContainer>
    )
  }
}

export default Sponsors;