import React from 'react';
import './Footer.css';
import ImgFooter from './imgFooter.jpg';
import Xacobeo from './xacobeo2021.jpg'

class Footer extends React.Component {
  render() {
    return (
      <footer className="Footer">
        <div className="xacobeo">
          <div className="hr-red"></div>
          <div className="container-fluid my-5">
            <div className="row">
              <div className="col-12">
                <h3 className="text-center w-100 mt-5">Prueba patrocinada por Xacobeo 2021</h3>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="image-wrapper text-center w-100 mt-3 mb-5">
                  <img src={Xacobeo} alt="xacobeo"/>
                </div>
              </div>
            </div>
          </div>
          <div className="hr-red"></div>
        </div>
        <div className="Footer-top">
          <div className="Footer-top-wrapper" 
            style={{
              backgroundImage: `url(${ImgFooter})`,
              backgroundSize: 'cover',
              backgroundRepeat: 'no-repeat',
              backgroundPosition: 'center',
              height: '350px',
              width: '100%'
            }}>
          </div>
        </div>
        <div className="Footer-bottom">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <p className="m0">© 2018 Copyright Globaldxt S.L.</p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;