import React from 'react';
import { Alert } from 'reactstrap';
import { FormattedMessage } from 'react-intl';

class AlertMessage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: true
    };

    this.onDismiss = this.onDismiss.bind(this);
  }

  onDismiss() {
    this.setState({ visible: false });
  }

  render() {
    return (
      <Alert color="danger" isOpen={this.state.visible} toggle={this.onDismiss}>
        <FormattedMessage id="alert" defaultMessage="APERTURA GENERAL DE INSCRIPCIONES 26 DE NOVIEMBRE" />
      </Alert>
    );
  }
}

export default AlertMessage;
