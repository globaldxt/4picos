import React from 'react';
import Header from '../_components/Header';
import Button from '../_components/Button';
import Footer from '../_components/Footer';
import ImageHero from './image1.jpg';
import './ContactPage.css';
import { FormattedMessage } from 'react-intl';
import Media from "react-media";
import HeaderMobile from '../_components/HeaderMobile';
import axios from 'axios';

const API_PATH = 'http://localhost:1992/var/www/pontevedra4picos.com/4picos/api/contact/index.php';

class ContactPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      message: '',
      mailSent: false,
      error: null
    }
  }

  handleFormSubmit = e => {
    e.preventDefault();
    axios({
        method: 'post',
        url: `${API_PATH}`,
        headers: { 'content-type': 'application/json' },
        data: this.state
      })
    .then(result => {
      this.setState( { 
        mailSent: result.data.sent
      })
      console.log(this.state);
    })
    .catch(error => this.setState( { error: error.message } ));
};

  render() {
    return (
      <div className="Contact">
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              <Header />
            ) : (
              <HeaderMobile />
            )}
        </Media>
        <section className="Contact-hero"
          style={{
            height: '350px',
            width: '100%',
            backgroundImage: `url(${ImageHero})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top'
          }}>
        </section>
        <section className="Contact-banner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <p>
                  <FormattedMessage id="contact.banner" defaultMessage="Inscripciones abiertas por aplazamiento de la edición 2020" />
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="Contact-info">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <h3 className="Contact-title">
                  <FormattedMessage id="contact.title" defaultMessage="Contacto" />
                </h3>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 col-lg-6 offset-lg-3">
                <article className="Contact-info-article">
                  <h4 className="Contact-info-title">
                    <FormattedMessage id="contact.info.title" defaultMessage="Puedes ponerte en contacto con nosotros rellenando este formulario" />
                  </h4>
                </article>
                <form action="#" className="Contact-form">
                  <div className="form-group">
                    <label htmlFor="email">
                      <FormattedMessage id="contact.email" defaultMessage="Correo electrónico" />
                    </label>
                    <input type="email" className="form-control" id="email" value={this.state.email} onChange={e => this.setState({ email: e.target.value })}/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="name">
                      <FormattedMessage id="contact.name" defaultMessage="Nombre" />
                    </label>
                    <input type="text" className="form-control" id="name" value={this.state.name} onChange={e => this.setState({ name: e.target.value })}/>
                  </div>
                  <div className="form-group">
                    <label htmlFor="message">
                      <FormattedMessage id="contact.message" defaultMessage="Mensaje" />
                    </label>
                    <textarea className="form-control" id="message" rows="3" onChange={e => this.setState({ message: e.target.value })} value={this.state.message}></textarea>
                  </div>
                  <div className="form-group">
                    <div className="Contact-button-wrapper">
                      <a className="Contact-button" target="_blank" rel="noopener noreferrer">
                        <input type="submit" onClick = {e => this.handleFormSubmit(e)} value="Enviar" />
                      </a>
                      <div> 
                        {this.state.mailSent  &&
                          <div className="sucsess">Thank you for contcting me.</div>
                        }
                        {this.state.error  &&
                          <div className="error">Sorry we had some problems.</div>
                        }
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              ''
            ) : (
              <section className="inscription-fixed-bar">
                <Button className="inscription-fixed" href="https://eventos.emesports.es/inscripcion/pontevedra-4picos-bike-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent" target="_blank" rel="noopener noreferrer">
                  Inscríbete
                </Button>
              </section>
            )}
        </Media>
      </div>
    );
  }
}

export default ContactPage;