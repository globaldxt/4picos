import React from 'react';
import Header from '../_components/Header';
import Button from '../_components/Button';
import Footer from '../_components/Footer';
import styled from 'styled-components';
import ImageHero from './image1.jpg';
import './SponsorsPage.css';
import { FormattedMessage } from 'react-intl';
import Media from "react-media";
import HeaderMobile from '../_components/HeaderMobile';
import sponsor1 from './img/xacobeo2021.jpg';
import sponsor2 from './img/concello-pontevedra.png';
import sponsor3 from './img/depo.jpg';
import sponsor4 from './img/froiz.jpg';
import sponsor5 from './img/bikeshop.png';
import sponsor6 from './img/vozysonido.png';
import sponsor7 from './img/gesmagal.png';
import sponsor8 from './img/rawRainbow.png';
import sponsor9 from './img/academy.png';
import sponsor10 from './img/caetano.png';
import sponsor11 from './img/beone.png';
import sponsor12 from './img/bifrutas.png';
import sponsor13 from './img/millabikes.png';
import sponsor14 from './img/pascual.png';
import sponsor15 from './img/spiuk.png';
import sponsor16 from './img/towcar.png';
import sponsor17 from './img/xeve.png';
import sponsor18 from './img/xsauce.png';
import sponsor19 from './img/ottobosley.png';
import sponsor20 from './img/pirelli.png';

const Sponsor = styled.img`
  max-width: 100%;
  height: auto;
  overflow: hidden;
`

const mainSponsors = [
  {
    name: 'Xacobeo2021',
    to: 'https://www.turismo.gal/espazo-institucional/xacobeo',
    image: sponsor1
  },
  {
    name: 'Concello Pontevedra',
    to: 'https://www.pontevedra.gal',
    image: sponsor2
  },
]

const sponsors = [
  {
    name: 'Deputación Pontevedra',
    to: 'https://www.depo.gal',
    image: sponsor3
  },
  {
    name: 'Froiz',
    to: 'https://www.froiz.com',
    image: sponsor4
  },
  {
    name: 'Bikeshop',
    to: 'https://www.bikeshop.es',
    image: sponsor5
  },
  {
    name: 'Voz y sonido',
    to: 'http://karpaproducciones.e.telefonica.net/',
    image: sponsor6
  },
  {
    name: 'Gesmagal',
    to: 'https://www.gesmagal.com',
    image: sponsor7
  },
  {
    name: 'Raw Rainbow',
    to: 'https://rawsuperdrink.com/',
    image: sponsor8
  },
  {
    name: 'TechBike Academy',
    to: '#',
    image: sponsor9
  },
  {
    name: 'Caetano Renault',
    to: 'https://www.caetanoformulagalicia.es/',
    image: sponsor10
  },
  {
    name: 'BeOne',
    to: 'https://beone.es/',
    image: sponsor11
  },
  {
    name: 'Bifrutas',
    to: 'https://beone.es/',
    image: sponsor12
  },
  {
    name: 'MillaBikes',
    to: 'https://millabikes.es/',
    image: sponsor13
  },
  {
    name: 'Pascual',
    to: 'https://millabikes.es/',
    image: sponsor14
  },
  {
    name: 'Spiuk',
    to: 'https://www.spiuk.com/',
    image: sponsor15
  },
  {
    name: 'Towcar',
    to: 'https://www.enganchesaragon.com/towcar.php',
    image: sponsor16
  },
  {
    name: 'Xeve',
    to: '#',
    image: sponsor17
  },
  {
    name: 'Xsauce',
    to: 'https://x-sauce.com/es/',
    image: sponsor18
  },
  {
    name: 'Ottobosley',
    to: 'https://www.ottobosley.com/',
    image: sponsor19
  },
  {
    name: 'Pirelli',
    to: 'https://www.pirelli.com/tyres/es-es/coche/home',
    image: sponsor20
  }
]

class SponsorsPage extends React.Component {
  render() {
    return (
      <div className="Sponsors">
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              <Header />
            ) : (
              <HeaderMobile />
            )}
        </Media>
        <section className="Sponsors-hero" 
          style={{
            height: '350px',
            width: '100%',
            backgroundImage: `url(${ImageHero})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'top'
          }}>
        </section>
        <section className="Sponsors-banner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <p>
                  <FormattedMessage id="Sponsors.banner" defaultMessage="Inscripciones abiertas
por aplazamiento de la
edición 2020" />
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="Sponsors-info">
          <div className="container-fluid">
          <div className="row">
              <div className="col-12">
                  <h3 className="Sponsors-title">
                    Sponsors
                  </h3>
              </div>
            </div>
            <div className="row">
              {mainSponsors.map(sponsor => {
                const {name, image, to} = sponsor;
                return (
                  <div className="col-12 col-sm-6 col-lg-6 my-3">
                    <a href={to} className="sponsor-wrapper p-4" target="_blank" rel="noopener noreferrer">
                      <Sponsor src={image} alt={name}></Sponsor>
                    </a>
                  </div>
                )
              })}
            </div>
            <div className="row">
              {sponsors.map(sponsor => {
                const {name, image, to} = sponsor;
                return (
                  <div className="col-12 col-sm-3 col-lg-3 my-3">
                    <a href={to} className="sponsor-wrapper second-sponsor p-5" target="_blank" rel="noopener noreferrer">
                      <Sponsor src={image} alt={name}></Sponsor>
                    </a>
                  </div>
                )
              })}
            </div>
          </div>
        </section>
        <Footer />
        <Media query={{ minWidth: 768 }}>
          {matches =>
            matches ? (
              ''
            ) : (
              <section className="inscription-fixed-bar">
                <Button className="inscription-fixed" href="https://eventos.emesports.es/inscripcion/pontevedra-4-picos-road-clasica-evaristo-portela-2020/inscripcion_datos/?iframe=1&lang=es&background=transparent" target="_blank" rel="noopener noreferrer">
                  Inscríbete
                </Button>
              </section>
            )}
        </Media>
      </div>
    );
  }
}

export default SponsorsPage;